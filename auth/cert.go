package auth

import (
	"github.com/hashicorp/vault/api"
	log "github.com/sirupsen/logrus"
	"gitlab.com/wieldes13/vaultbot/cli"
)

const (
	// MethodCert authenticates to vault via client cert
	MethodCert = "cert"
)

// Cert authenticates to vault via the cert auth backend
func Cert(options cli.Options, client *api.Client) {
	// check if necessary options are set
	if options.Vault.ClientCert == "" {
		log.Fatalf("option ´vault_client_cert´ must be set when using the Cert backend")
	}

	if options.Vault.ClientKey == "" {
		log.Fatalf("option ´vault_client_key´ must be set when using the Cert backend")
	}

	loginData := make(map[string]interface{})
	if options.Vault.CertificateRole == "" {
		loginData = nil
	} else {
		loginData["name"] = options.Vault.CertificateRole
	}

	resp, err := client.Logical().Write("auth/cert/login", loginData)
	if err != nil {
		log.Fatalf("Error authenticating to vault via Cert backend: %s", err)
	}

	// retrieve the token
	var token string

	if token, err = resp.TokenID(); err != nil {
		log.Fatalf("Error retrieving token after authentication to vault via Cert authentication: %s", err)
	}

	client.SetToken(token)
}
