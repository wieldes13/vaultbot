package auth

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	log "github.com/sirupsen/logrus"

	"github.com/aws/aws-sdk-go/aws/ec2metadata"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
	"github.com/hashicorp/vault/api"
	"gitlab.com/wieldes13/vaultbot/cli"
)

const (
	// MethodAWSIAM authenticates to vault via AWS IAM Auth
	MethodAWSIAM = "aws-iam"

	// MethodAWSEC2 authenticates to vault via AWS EC2 Auth
	MethodAWSEC2 = "aws-ec2"

	// AWSAuthErrorMessage is the default error message for AWS IAM Authentication
	AWSAuthErrorMessage = "Error authenticating to vault via AWS IAM authentication"

	// AWSAuthHeaderKey is the header key used during AWS IAM authentication
	AWSAuthHeaderKey = "X-Vault-AWS-IAM-Server-ID"

	// NonceMaxBytes is the maximum number of bytes that will be read from the nonce file
	NonceMaxBytes = 1024
)

// AWSEC2 authenticates to vault via the AWS EC2 authentication method
func AWSEC2(options cli.Options, client *api.Client) {
	ec2m := ec2metadata.New(session.New())
	data, _ := ec2m.GetDynamicData("/instance-identity/pkcs7")

	loginData := make(map[string]interface{})
	var nonceFile *os.File
	var writeNonce = false

	if options.Vault.AWSAuthNonce != "" && options.Vault.AWSAuthNoncePath != "" {
		log.Fatalln("AWS EC2 Auth requires that only one of AWSAuthNonce, AWSAuthNoncePath is set")
	}

	if options.Vault.AWSAuthNonce == "" {
		if options.Vault.AWSAuthNoncePath == "" {
			log.Fatalf("AWS EC2 Auth requires one of the following arguments: AWSAuthNonce, AWSAuthNoncePath")
		} else {
			// Attempt to read the nonce from file
			var nonce string
			nonce, nonceFile = readNonceFromFile(options)
			defer nonceFile.Close()

			if nonce == "" {
				writeNonce = true
			} else {
				loginData["nonce"] = nonce
			}
		}
	} else {
		loginData["nonce"] = options.Vault.AWSAuthNonce
	}

	loginData["pkcs7"] = data
	loginData["role"] = options.Vault.AWSAuthRole

	// authenticate to vault
	resp, err := client.Logical().Write(fmt.Sprintf("auth/%s/login", options.Vault.AWSAuthMount), loginData)
	if err != nil {
		log.Fatalf("%s: %s", AWSAuthErrorMessage, err)
	}

	// retrieve the token
	var token string

	if token, err = resp.TokenID(); err != nil {
		log.Fatalf("Error retrieving token after authentication to vault via AWS EC2 authentication: %s", err)
	}

	if options.Vault.AWSAuthNoncePath != "" && writeNonce {
		var nonce interface{}
		var ok bool

		if metadata, err := resp.TokenMetadata(); err == nil {
			if nonce, ok = metadata["nonce"]; !ok {
				log.Fatalf("Could not find AWS EC2 nonce from Vault's authentication response. Metadata: %v", metadata)
			}
		} else {
			log.Fatalf("Error retrieving token metadata after authentication to vault via AWS EC2 Auth: %s", err)
		}

		if nonceString, ok := nonce.(string); ok {
			_, writeErr := nonceFile.WriteString(nonceString)
			if writeErr != nil {
				log.Fatalf("Unable to write nonce %s to file. Error: %s", nonceString, writeErr)
			}

			nonceFile.Sync()
			log.Printf("Wrote nonce to file: %s", options.Vault.AWSAuthNoncePath)
		} else {
			log.Fatalf("Could not convert AWS EC2 nonce in Vault's authentication response to string. Value was: %v", nonce)
		}
	}

	client.SetToken(token)
}

// AWSIAM authenticates to vault via AWS IAM authentication
func AWSIAM(options cli.Options, client *api.Client) {
	// construct sts session
	stsSession := session.New()

	var params *sts.GetCallerIdentityInput
	svc := sts.New(stsSession)
	stsRequest, _ := svc.GetCallerIdentityRequest(params)

	// ADD VAULT AWS IAM auth header value
	if options.Vault.AWSAuthHeader != "" {
		stsRequest.HTTPRequest.Header.Add(AWSAuthHeaderKey, options.Vault.AWSAuthHeader)
	}
	stsRequest.Sign()

	// Extract values from request
	headersJSON, err := json.Marshal(stsRequest.HTTPRequest.Header)
	if err != nil {
		log.Fatalf("%s: %s", AWSAuthErrorMessage, err)
	}
	requestBody, err := ioutil.ReadAll(stsRequest.HTTPRequest.Body)
	if err != nil {
		log.Fatalf("%s: %s", AWSAuthErrorMessage, err)
	}

	loginData := make(map[string]interface{})
	loginData["iam_http_request_method"] = stsRequest.HTTPRequest.Method
	loginData["iam_request_url"] = base64.StdEncoding.EncodeToString([]byte(stsRequest.HTTPRequest.URL.String()))
	loginData["iam_request_headers"] = base64.StdEncoding.EncodeToString(headersJSON)
	loginData["iam_request_body"] = base64.StdEncoding.EncodeToString(requestBody)
	loginData["role"] = options.Vault.AWSAuthRole

	// authenticate to vault
	resp, err := client.Logical().Write(fmt.Sprintf("auth/%s/login", options.Vault.AWSAuthMount), loginData)
	if err != nil {
		log.Fatalf("%s: %s", AWSAuthErrorMessage, err)
	}

	// retrieve the token
	var token string

	if token, err = resp.TokenID(); err != nil {
		log.Fatalf("Error retrieving token after authentication to vault via AWS IAM authentication: %s", err)
	}

	client.SetToken(token)
}

// readNonceFromFile attempts to read the nonce from a file.
// If the file does not exist yet, it will get created and it's file handle is returned.
func readNonceFromFile(options cli.Options) (string, *os.File) {
	if _, err := os.Stat(options.Vault.AWSAuthNoncePath); err == nil {

		nonceFile, err := os.OpenFile(options.Vault.AWSAuthNoncePath, os.O_RDONLY, 0)

		if err != nil {
			log.Fatalf("Error opening nonce file: %s", err)
		}

		// Read nonce
		nonceBytes := make([]byte, NonceMaxBytes)
		numRead, err := nonceFile.Read(nonceBytes)

		if err != nil {
			log.Fatalf("Error reading nonce from file: %s", err)
		}

		if numRead == 0 {
			log.Fatalf("Nonce length exceeded max length of %d bytes", NonceMaxBytes)

		}

		return string(nonceBytes[0:numRead]), nil
	} else if os.IsNotExist(err) {

		// If the nonce file does not exist yet, create it
		if nonceFile, err := os.OpenFile(options.Vault.AWSAuthNoncePath, os.O_CREATE|os.O_EXCL|os.O_WRONLY, 0660); err != nil {
			log.Fatalf("Could not create nonce file for writing. Will not attempt to authenticate to Vault via AWS EC2 Auth. Error: %s", err)
		} else {
			return "", nonceFile
		}
	}
	return "", nil
}
