package auth

import (
	log "github.com/sirupsen/logrus"

	"github.com/hashicorp/vault/api"
	"gitlab.com/wieldes13/vaultbot/cli"
)

// MethodToken authenticates to vault via token
const MethodToken = "token"

// CheckAuthentication verifies that the connection to vault is setup correctly by retrieving information about the configured token
func CheckAuthentication(client *api.Client) {
	tokenInfo, tokenErr := client.Auth().Token().LookupSelf()
	if tokenErr != nil {
		log.Fatalf("Error connecting to vault: %s", tokenErr)
	}

	tokenPolicies, polErr := tokenInfo.TokenPolicies()
	if polErr != nil {
		log.Fatalf("Error looking up token policies: %s", tokenErr)
	}
	log.Printf("Successfully authenticated to vault. Got token policies: %s", tokenPolicies)
}

// Token authenticates to vault via token
func Token(options cli.Options, client *api.Client) {
	client.SetToken(options.Vault.Token)
}
