package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"github.com/hashicorp/vault/api"
	"github.com/hashicorp/vault/sdk/helper/parseutil"
	log "github.com/sirupsen/logrus"
	"gitlab.com/wieldes13/vaultbot/auth"
	"gitlab.com/wieldes13/vaultbot/cli"
)

// renewSelf refreshes the lease of the specified token
func renewSelf(client *api.Client, options cli.Options) {
	responseData, err := client.Logical().Write("/auth/token/renew-self", nil)
	if err != nil {
		log.Fatalf("Unable to renew token: %s", err)
	}
	time, _ := responseData.TokenTTL()
	log.Printf("Renewed token, new ttl: %s", time)
}

// createClient configures the connection to vault
func createClient(options cli.Options) *api.Client {

	// pre parse some options
	clientTimeout, err := parseutil.ParseDurationSecond(options.Vault.ClientTimeout)
	if err != nil {
		log.Fatalf("Could not parse client_timeout: %v", options.Vault.ClientTimeout)
	}

	config := api.Config{
		Address:    options.Vault.Address,
		MaxRetries: options.Vault.MaxRetries,
		Timeout:    clientTimeout,
	}

	config.ConfigureTLS(&api.TLSConfig{
		CACert:        options.Vault.CACert,
		CAPath:        options.Vault.CAPath,
		ClientCert:    options.Vault.ClientCert,
		ClientKey:     options.Vault.ClientKey,
		TLSServerName: options.Vault.TLSServerName,
		Insecure:      options.Vault.Insecure,
	})

	client, err := api.NewClient(&config)
	if err != nil {
		log.Fatalf("Error initializing client: %s", err.Error())
	}

	switch options.Vault.AuthMethod {
	case auth.MethodCert:
		auth.Cert(options, client)
	case auth.MethodToken:
		auth.Token(options, client)
	case auth.MethodAWSIAM:
		auth.AWSIAM(options, client)
	case auth.MethodAWSEC2:
		auth.AWSEC2(options, client)
	case auth.MethodAppRole:
		auth.AppRole(options, client)
	default:
		log.Fatalf("Error: vault_auth_method should be one of [cert, approle, token, aws-iam, aws-ec2], got: %s", options.Vault.AuthMethod)
	}

	auth.CheckAuthentication(client)

	return client
}

// executeRenewHook performs a specified os command
func executeRenewHook(command string) {
	args := strings.Fields(command)
	bin := args[0]
	args = args[1:len(args)]

	cmd := exec.Command(bin, args...)
	log.Printf("Running renew-hook: %s", command)
	err := cmd.Run()

	if err != nil {
		log.Fatalf("Unable to run renew-hook: %s", err)
	}
	log.Printf("Executed renew-hook successfully!")
}

// userConfirmation prompts the user with a message and returns true or false based on the users response
func userConfirmation(prompt string) bool {
	reader := bufio.NewReader(os.Stdin)

	for i := 0; i < 4; i++ {
		fmt.Printf("%s [y/n]: ", prompt)

		input, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}

		input = strings.ToLower(strings.TrimSpace(input))

		if input == "y" {
			return true
		} else if input == "n" {
			return false
		}
	}

	return false
}
