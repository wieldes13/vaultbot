package main

import (
	"crypto/md5"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"io"
	"io/ioutil"
	"math/big"
	"net"
	"os"
	"testing"
	"time"

	"github.com/hashicorp/vault/sdk/helper/certutil"
	log "github.com/sirupsen/logrus"
	"gitlab.com/wieldes13/vaultbot/auth"
	"gitlab.com/wieldes13/vaultbot/cli"

	//JKS extension dependencies
	"github.com/kami-zh/go-capturer"
)

var dirPath string
var certPath string
var caPath string
var JKSPath string
var privkeyPath string

func TestMain(m *testing.M) {
	// create test cert
	dirPath = setupTestFolder()
	certPath = fmt.Sprintf("%s/cert.pem", dirPath)
	caPath = fmt.Sprintf("%s/ca.pem", dirPath)
	JKSPath = fmt.Sprintf("%s/jks.jks", dirPath)
	privkeyPath = fmt.Sprintf("%s/key.pem", dirPath)

	// run tests
	code := m.Run()

	// clean up on test exit
	//defer os.RemoveAll(dirPath)
	os.Exit(code)
}

func TestSetupLogging(t *testing.T) {
	setupLogging(cli.Options{Logfile: fmt.Sprintf("%s/test.log", dirPath)})
}

func TestCheckAuthentication(t *testing.T) {
	opts := cli.Options{}

	opts.Vault.Address = "http://vault:1234"
	opts.Vault.AuthMethod = "token"
	opts.Vault.Token = "myroot"

	client := createClient(opts)
	auth.CheckAuthentication(client)
}

func TestAppRole(t *testing.T) {
	opts := cli.Options{}

	opts.Vault.Address = "http://vault:1234"
	opts.Vault.AuthMethod = "token"
	opts.Vault.Token = "myroot"
	opts.Vault.AppRoleMount = "approle"

	// setup approle backend for testing
	client := createClient(opts)

	resp, err := client.Logical().Write(fmt.Sprintf("auth/%s/role/my-role/secret-id", opts.Vault.AppRoleMount), nil)
	if err != nil {
		log.Fatalf("Error creating AppRole secret-id: %s", err)
	}

	opts.Vault.AppRoleSecretID = resp.Data["secret_id"].(string)

	resp, err = client.Logical().Read(fmt.Sprintf("auth/%s/role/my-role/role-id", opts.Vault.AppRoleMount))
	if err != nil {
		log.Fatalf("Error reading AppRole role-id: %s", err)
	}

	opts.Vault.AppRoleRoleID = resp.Data["role_id"].(string)

	// test the newly setup AppRole authentication backend
	opts.Vault.AuthMethod = "approle"
	auth.AppRole(opts, client)
	auth.CheckAuthentication(client)
}

func TestEndToEnd(t *testing.T) {
	opts := cli.Options{}

	opts.PKI.CertPath = fmt.Sprintf("%s/e2eCert.pem", dirPath)
	opts.PKI.CAChainPath = fmt.Sprintf("%s/e2eCA.pem", dirPath)
	opts.PKI.PrivKeyPath = fmt.Sprintf("%s/e2eKey.pem", dirPath)
	opts.Vault.Address = "http://vault:1234"
	opts.Vault.AuthMethod = "token"
	opts.Vault.Token = "myroot"

	opts.PKI.CommonName = "vaultbot.test"
	opts.PKI.AltNames = "testing.com"
	opts.PKI.IPSans = "127.0.0.1,192.168.0.1"
	opts.PKI.Mount = "pki"
	opts.PKI.RoleName = "example-dot-com"
	opts.PKI.RenewPercent = 0.7

	opts.Verbose = true
	opts.Logfile = "./vaulbottest/log.log"

	t.Log("Test #1")
	// initial run test
	out := capturer.CaptureStdout(func() {
		run(opts)
	})

	t.Log(out)

	cert := readCurrentCertificate(opts)
	if cert == nil {
		t.Fatal("Failed end to end test, no certificate found")
	}

	// check if file was not modified, because not yet expired
	oldHash := getFileHash(fmt.Sprintf("%s/e2eCert.pem", dirPath))

	// run again, should not modify
	t.Log("Test #2")
	out = capturer.CaptureStdout(func() {
		run(opts)
	})

	t.Log(out)

	newHash := getFileHash(fmt.Sprintf("%s/e2eCert.pem", dirPath))
	if oldHash != newHash {
		t.Fatalf("Error in end to end test, certificate was modified when it was not expired!")
	}

	// run again, should modify now
	opts.PKI.RenewPercent = 0.000000001
	opts.PKI.PEMBundlePath = fmt.Sprintf("%s/e2eBundle.pem", dirPath)
	opts.RenewHook = fmt.Sprintf("touch %s/test.txt", dirPath)
	t.Log("Test #3")

	out = capturer.CaptureStdout(func() {
		run(opts)
	})

	t.Log(out)

	renewHash := getFileHash(fmt.Sprintf("%s/e2eCert.pem", dirPath))
	if renewHash == newHash {
		t.Fatalf("Error in end to end test, certificate was not modified, although renew_percent should trigger!")
	}
}

func TestExecuteRenewHook(t *testing.T) {
	executeRenewHook(fmt.Sprintf("touch %s/test.txt", dirPath))

	if _, err := os.Stat(fmt.Sprintf("%s/test.txt", dirPath)); os.IsNotExist(err) {
		t.Fatal("Renew-hook test failed!")
	}
}

func TestRenewSelf(t *testing.T) {
	opts := cli.Options{}
	opts.Vault.Address = "http://vault:1234"
	opts.Vault.AuthMethod = "token"
	opts.Vault.Token = "myPeriodicToken"

	client := createClient(opts)
	renewSelf(client, opts)
}

func TestRequestCertificate(t *testing.T) {

	cn := "vaultbot.test"
	altNames := "vaultbot.test"
	ipSANS := "127.0.0.1,192.168.0.1"

	opts := cli.Options{}

	opts.PKI.CertPath = certPath
	opts.PKI.CAChainPath = caPath
	opts.PKI.PrivKeyPath = privkeyPath
	opts.Vault.Address = "http://vault:1234"
	opts.Vault.AuthMethod = "token"
	opts.Vault.Token = "myroot"

	opts.PKI.CommonName = cn
	opts.PKI.AltNames = altNames
	opts.PKI.IPSans = ipSANS
	opts.PKI.Mount = "pki"
	opts.PKI.RoleName = "example-dot-com"

	client := createClient(opts)
	cert := requestCertificate(client, opts)
	if cert == nil {
		t.Fatal("Error requesting certificate")
	}

	writeCertificateData(cert, opts)
	certRead := readCurrentCertificate(opts)
	if certRead.Subject.CommonName != cn {
		t.Fatal("Received CN does not match requested CN!")
	}

}

func TestHasCertificateDataChanged(t *testing.T) {
	opts := cli.Options{}
	opts.PKI.CertPath = certPath
	createTestCert(certPath, "VaultBot.Test", []net.IP{net.ParseIP("127.0.0.1")}, []string{"testing.test", "helloworld.com", "VaultBot.Test"}, time.Now().Add(1*time.Hour))

	readCert := readCurrentCertificate(opts)

	// nothing changed, match created cert
	opts.PKI.CommonName = "VaultBot.Test"
	opts.PKI.IPSans = "127.0.0.1"
	opts.PKI.AltNames = "testing.test,helloworld.com"

	changed := hasCertificateDataChanged(readCert, opts)
	if changed {
		t.Fatal("Certificate data changed, when it shouldnt!")
	}

	// IP SANs len changed, does not match created cert
	opts.PKI.IPSans = "127.0.0.1,192.168.0.1"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("IP SANs len did not changed, when it should!")
	}

	// IP SANs fields changed, does not match created cert
	opts.PKI.IPSans = "192.168.0.1"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("IP SANs field did not changed, when it should!")
	}

	// dns alt names len changed, does not match created cert
	opts.PKI.AltNames = "helloworld.com"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("Dns alt names len did not changed, when it should!")
	}

	// dns alt names fields changed, does not match created cert
	opts.PKI.AltNames = "helloworld.com,changed.com"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("Dns alt names field did not changed, when it should!")
	}

	// common name changed, does not match created cert
	opts.PKI.AltNames = "testing.test,helloworld.com"
	opts.PKI.CommonName = "VaultBot.Changed"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("Certificate data did not change, when it should!")
	}
}

func TestHasCertificateDataChangedNoSans(t *testing.T) {
	opts := cli.Options{}
	opts.PKI.CertPath = certPath
	createTestCert(certPath, "VaultBot.Test", []net.IP{}, []string{"VaultBot.Test"}, time.Now().Add(1*time.Hour))

	readCert := readCurrentCertificate(opts)

	// nothing changed, match created cert
	opts.PKI.CommonName = "VaultBot.Test"
	opts.PKI.IPSans = ""
	opts.PKI.AltNames = ""

	changed := hasCertificateDataChanged(readCert, opts)
	if changed {
		t.Fatal("Certificate data changed, when it shouldnt!")
	}

	// IP SANs len changed, does not match created cert
	opts.PKI.IPSans = "127.0.0.1,192.168.0.1"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("IP SANs len did not changed, when it should!")
	}

	// IP SANs fields changed, does not match created cert
	opts.PKI.IPSans = "192.168.0.1"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("IP SANs field did not changed, when it should!")
	}

	// dns alt names len changed, does not match created cert
	opts.PKI.AltNames = "helloworld.com"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("Dns alt names len did not changed, when it should!")
	}

	// dns alt names fields changed, does not match created cert
	opts.PKI.AltNames = "helloworld.com,changed.com"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("Dns alt names field did changed, when it should!")
	}

	// common name changed, does not match created cert
	opts.PKI.AltNames = "testing.test,helloworld.com"
	opts.PKI.CommonName = "VaultBot.Changed"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("Certificate data did not change, when it should!")
	}
}

func TestCertificateRenewalDue(t *testing.T) {
	opts := cli.Options{}
	opts.PKI.CertPath = certPath
	createTestCert(certPath, "VaultBot.Test", []net.IP{net.ParseIP("127.0.0.1")}, []string{"testing.test"}, time.Now().Add(1*time.Hour))

	// test certificate reading
	readCert := readCurrentCertificate(opts)

	if readCert == nil {
		t.Fatalf("Failed reading test certificate")
	}
	log.Println(readCert.Subject.CommonName)

	// test certificate expiry check
	// not yet due
	opts.PKI.RenewTime = "1m"
	if isCertificateRenewalDue(readCert, opts) {
		t.Fatalf("Certificate expiry check failed. Marked as due for renewal when it should not!")
	}

	// due
	opts.PKI.RenewTime = "2h"
	if !isCertificateRenewalDue(readCert, opts) {
		t.Fatalf("Certificate expiry check failed. Not marked as due for renewal when it should be!")
	}

	// forece renew
	opts.PKI.ForceRenew = true
	if !isCertificateRenewalDue(readCert, opts) {
		t.Fatalf("Certificate expiry check failed. Force renewal flag not working!")
	}
}

func TestReadNonExistentJKS(t *testing.T) {
	keyStore := readKeyStore("my/jks.jks", []byte("ChangeIt"))

	if keyStore != nil {
		t.Fatalf("Error with non-existent JKS, should be nil result")
	}
}

func TestWriteNewJKS(t *testing.T) {

	opts := cli.Options{}

	opts.Vault.Address = "http://vault:1234"
	opts.Vault.AuthMethod = "token"
	opts.Vault.Token = "myroot"

	opts.PKI.CommonName = "vaultbot.test"
	opts.PKI.AltNames = "testing.com"
	opts.PKI.IPSans = "127.0.0.1,192.168.0.1"
	opts.PKI.Mount = "pki"
	opts.PKI.RoleName = "example-dot-com"
	opts.PKI.RenewPercent = 0.7

	opts.PKI.JKSExport = true
	opts.PKI.JKSPath = JKSPath
	opts.PKI.JKSPassword = "ChangeIt"
	opts.PKI.JKSCertAlias = "Cert"
	opts.PKI.JKSCAChainAlias = "Chain"
	opts.PKI.JKSPrivKeyAlias = "Key"

	opts.Verbose = true
	opts.Logfile = "./testoutput/log.log"

	t.Log("Test Init")

	client := createClient(opts)
	cert := requestCertificate(client, opts)

	if cert == nil {
		t.Fatalf("Error initating certificate request for JKS")
	}

	t.Log("Test #1")

	writeJKSCertificateData(cert, opts)
	keyStore := readKeyStore(opts.PKI.JKSPath, []byte(opts.PKI.JKSPassword))

	if keyStore == nil {
		t.Fatalf("JKS file has been not been created")
	} else {
		t.Log("Test #1: File has been created sucessfuly")
	}

	t.Log("Test #2")

	readCert := readJKSCurrentCertificate(opts)

	if readCert == nil {
		t.Fatalf("Certificate cannot be read from JKS file")
	} else {
		t.Log("Test #2: Certificate has been read from JKS file successfully")
	}

	t.Log("Test #3")

	if readCert.Subject.CommonName == opts.PKI.CommonName {
		t.Log("Test #3: Certificate data has been validated from JKS file successfully")
	} else {
		t.Fatalf("Certificate from JKS file is not the created")
	}

}

func TestDataHasChangedFromJKS(t *testing.T) {
	opts := cli.Options{}

	opts.Vault.Address = "http://vault:1234"
	opts.Vault.AuthMethod = "token"
	opts.Vault.Token = "myroot"

	opts.PKI.CommonName = "vaultbot.test"
	opts.PKI.Mount = "pki"
	opts.PKI.RoleName = "example-dot-com"
	opts.PKI.RenewPercent = 0.7

	opts.PKI.JKSExport = true
	opts.PKI.JKSPath = fmt.Sprintf("%s/jks-changed.jks", dirPath)
	opts.PKI.JKSPassword = "ChangeIt"
	opts.PKI.JKSCertAlias = "Cert"
	opts.PKI.JKSCAChainAlias = "Chain"
	opts.PKI.JKSPrivKeyAlias = "Key"

	opts.Verbose = true
	opts.Logfile = "./testoutput/log.log"

	t.Log("Test Init")

	client := createClient(opts)
	cert := requestCertificate(client, opts)

	if cert == nil {
		t.Fatalf("Error initating certificate request for JKS")
	}

	writeJKSCertificateData(cert, opts)

	readCert := readJKSCurrentCertificate(opts)

	t.Log("Test #1")

	changed := hasCertificateDataChanged(readCert, opts)
	if changed {
		t.Fatal("Certificate data changed, when it shouldnt!")
	} else {
		t.Log("Test #1: OK, data has not changed")
	}

	t.Log("Test #2: change IPSans")

	// IP SANs len changed, does not match created cert
	opts.PKI.IPSans = "127.0.0.1,192.168.0.1"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("IP SANs len did not changed, when it should!")
	} else {
		t.Log("Test #2: OK, data has changed")
	}

	t.Log("Test #3: change IPSans")

	// IP SANs fields changed, does not match created cert
	opts.PKI.IPSans = "192.168.0.1"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("IP SANs field did not changed, when it should!")
	} else {
		t.Log("Test #3: OK, data has changed")
	}

	t.Log("Test #4: change DNS alt names")

	// dns alt names len changed, does not match created cert
	opts.PKI.AltNames = "helloworld.com"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("Dns alt names len did not changed, when it should!")
	} else {
		t.Log("Test #4: OK, data has changed")
	}

	t.Log("Test #5: change DNS alt names")

	// dns alt names fields changed, does not match created cert
	opts.PKI.AltNames = "helloworld.com,changed.com"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("Dns alt names field did not changed, when it should!")
	} else {
		t.Log("Test #5: OK, data has changed")
	}

	t.Log("Test #6: change common name")

	// common name changed, does not match created cert
	opts.PKI.AltNames = "testing.test,helloworld.com"
	opts.PKI.CommonName = "VaultBot.Changed"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("Certificate data did not change, when it should!")
	} else {
		t.Log("Test #6: OK, data has changed")
	}
}

func TestHasDataChangedNoSansFromJKS(t *testing.T) {
	opts := cli.Options{}

	opts.Vault.Address = "http://vault:1234"
	opts.Vault.AuthMethod = "token"
	opts.Vault.Token = "myroot"

	opts.PKI.CommonName = "vaultbot.test"
	opts.PKI.Mount = "pki"
	opts.PKI.RoleName = "example-dot-com"
	opts.PKI.RenewPercent = 0.7

	opts.PKI.JKSExport = true
	opts.PKI.JKSPath = fmt.Sprintf("%s/jks-changed2.jks", dirPath)
	opts.PKI.JKSPassword = "ChangeIt"
	opts.PKI.JKSCertAlias = "Cert"
	opts.PKI.JKSCAChainAlias = "Chain"
	opts.PKI.JKSPrivKeyAlias = "Key"

	opts.Verbose = true
	opts.Logfile = "./testoutput/log.log"

	t.Log("Test Init")

	client := createClient(opts)
	cert := requestCertificate(client, opts)

	if cert == nil {
		t.Fatalf("Error initating certificate request for JKS")
	}

	writeJKSCertificateData(cert, opts)

	readCert := readJKSCurrentCertificate(opts)

	t.Log("Test #1")

	// nothing changed, match created cert
	opts.PKI.CommonName = "VaultBot.Test"
	opts.PKI.IPSans = ""
	opts.PKI.AltNames = ""

	changed := hasCertificateDataChanged(readCert, opts)
	if changed {
		t.Fatal("Certificate data changed, when it shouldnt!")
	} else {
		t.Log("Test #1: OK, data has not changed")
	}

	t.Log("Test #2")

	// IP SANs len changed, does not match created cert
	opts.PKI.IPSans = "127.0.0.1,192.168.0.1"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("IP SANs len did not changed, when it should!")
	} else {
		t.Log("Test #2: OK, data has changed")
	}

	t.Log("Test #3")

	// IP SANs fields changed, does not match created cert
	opts.PKI.IPSans = "192.168.0.1"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("IP SANs field did not changed, when it should!")
	} else {
		t.Log("Test #3: OK, data has changed")
	}

	t.Log("Test #4")

	// dns alt names len changed, does not match created cert
	opts.PKI.AltNames = "helloworld.com"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("Dns alt names len did not changed, when it should!")
	} else {
		t.Log("Test #4: OK, data has changed")
	}

	t.Log("Test #5")

	// dns alt names fields changed, does not match created cert
	opts.PKI.AltNames = "helloworld.com,changed.com"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("Dns alt names field did not changed, when it should!")
	} else {
		t.Log("Test #5: OK, data has changed")
	}

	t.Log("Test #6")

	// common name changed, does not match created cert
	opts.PKI.AltNames = "testing.test,helloworld.com"
	opts.PKI.CommonName = "VaultBot.Changed"
	changed = hasCertificateDataChanged(readCert, opts)
	if !changed {
		t.Fatal("Certificate data did not change, when it should!")
	} else {
		t.Log("Test #6: OK, data has changed")
	}
}

func TestJKSEndToEnd(t *testing.T) {
	opts := cli.Options{}

	opts.Vault.Address = "http://vault:1234"
	opts.Vault.AuthMethod = "token"
	opts.Vault.Token = "myroot"

	opts.PKI.CommonName = "vaultbot.test"
	opts.PKI.AltNames = "testing.com"
	opts.PKI.IPSans = "127.0.0.1,192.168.0.1"
	opts.PKI.Mount = "pki"
	opts.PKI.RoleName = "example-dot-com"
	opts.PKI.RenewPercent = 0.7

	opts.PKI.JKSExport = true
	opts.PKI.JKSPath = fmt.Sprintf("%s/jks-e2e.jks", dirPath)
	opts.PKI.JKSPassword = "ChangeIt"
	opts.PKI.JKSCertAlias = "Cert"
	opts.PKI.JKSCAChainAlias = "Chain"
	opts.PKI.JKSPrivKeyAlias = "Key"

	opts.Verbose = true
	opts.Logfile = "./testoutput/log.log"

	t.Log("Test #1")
	// initial run test
	out := capturer.CaptureStdout(func() {
		run(opts)
	})

	t.Log(out)

	cert := readJKSCurrentCertificate(opts)
	if cert == nil {
		t.Fatal("Failed end to end test, no certificate found")
	} else {
		t.Log("Test #1: Certificate can be read")
	}

	// check if file was not modified, because not yet expired
	oldCert := cert

	// run again, should not modify
	t.Log("Test #2")
	out = capturer.CaptureStdout(func() {
		run(opts)
	})

	t.Log(out)

	cert = readJKSCurrentCertificate(opts)

	newCert := cert

	if !(oldCert.Equal(newCert)) {
		t.Fatalf("Error in end to end test, certificate was modified when it was not expired!")
	} else {
		t.Log("Test #2: OK, data has not changed")
	}

	// run again, should modify now
	opts.PKI.RenewPercent = 0.000000001
	opts.RenewHook = fmt.Sprintf("touch %s/test.txt", dirPath)
	t.Log("Test #3")

	out = capturer.CaptureStdout(func() {
		run(opts)
	})

	t.Log(out)

	cert = readJKSCurrentCertificate(opts)

	renewCert := cert

	if renewCert.Equal(newCert) {
		t.Fatalf("Error in end to end test, certificate was not modified, although renew_percent should trigger!")
	} else {
		t.Log("Test #3: OK, data has changed")
	}
}

// creates a temp folder to store certificates
func setupTestFolder() string {
	dir := "./testoutput"
	return dir
}

// creates a test certificate with specified options
func createTestCert(path string, commonName string, ipSANs []net.IP, dnsSANs []string, notAfter time.Time) {
	RSAPrivateKey, _ := rsa.GenerateKey(rand.Reader, 1024)
	notBefore := time.Now()
	template := x509.Certificate{
		SerialNumber: new(big.Int).Lsh(big.NewInt(1), 128),
		Subject: pkix.Name{
			CommonName:   commonName,
			Organization: []string{"Acme Co"},
		},
		IsCA:                  true,
		IPAddresses:           ipSANs,
		DNSNames:              dnsSANs,
		NotBefore:             notBefore,
		NotAfter:              notAfter,
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	certBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, &RSAPrivateKey.PublicKey, RSAPrivateKey)
	if err != nil {
		log.Fatalf("Failed to create certificate: %s", err)
	}

	certOut, err := os.Create(path)
	if err != nil {
		log.Fatalf("failed to open %s for writing: %s", path, err)
	}
	pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: certBytes})
	certOut.Close()
}

func TestPKCS1PrivateKeyFormat(t *testing.T) {

	cn := "vaultbot.test"
	altNames := "vaultbot.test"
	ipSANS := "127.0.0.1,192.168.0.1"

	opts := cli.Options{}

	opts.PKI.CertPath = certPath
	opts.PKI.CAChainPath = caPath
	opts.PKI.PrivKeyPath = privkeyPath
	opts.Vault.Address = "http://vault:1234"
	opts.Vault.AuthMethod = "token"
	opts.Vault.Token = "myroot"

	opts.PKI.CommonName = cn
	opts.PKI.AltNames = altNames
	opts.PKI.IPSans = ipSANS
	opts.PKI.Mount = "pki"
	opts.PKI.RoleName = "example-dot-com"

	client := createClient(opts)
	cert := requestCertificate(client, opts)
	if cert == nil {
		t.Fatal("Error requesting certificate")
	}

	writeCertificateData(cert, opts)
	PrivateKeyBlock := readCurrentKeyBlock(opts)
	if !isPKCS1Block(PrivateKeyBlock.Type) {
		log.Fatalf("Error on private key block type")
	}
	PrivateKeyBytes := PrivateKeyBlock.Bytes
	_, err := x509.ParsePKCS1PrivateKey(PrivateKeyBytes)
	if err != nil {
		log.Fatalf("Unable to parse private RSA key")
	}
}

func TestPKCS8PrivateKeyFormat(t *testing.T) {

	cn := "vaultbot.test"
	altNames := "vaultbot.test"
	ipSANS := "127.0.0.1,192.168.0.1"

	opts := cli.Options{}

	opts.PKI.CertPath = certPath
	opts.PKI.CAChainPath = caPath
	opts.PKI.PrivKeyPath = privkeyPath
	opts.Vault.Address = "http://vault:1234"
	opts.Vault.AuthMethod = "token"
	opts.Vault.Token = "myroot"

	opts.PKI.CommonName = cn
	opts.PKI.AltNames = altNames
	opts.PKI.IPSans = ipSANS
	opts.PKI.Mount = "pki"
	opts.PKI.RoleName = "example-dot-com"
	opts.PKI.PrivateKeyFormat = "pkcs8"

	client := createClient(opts)
	cert := requestCertificate(client, opts)
	if cert == nil {
		t.Fatal("Error requesting certificate")
	}

	writeCertificateData(cert, opts)
	PrivateKeyBlock := readCurrentKeyBlock(opts)
	if !isPKCS8Block(PrivateKeyBlock.Type) {
		log.Fatalf("Error on private key block type")
	}
	PrivateKeyBytes := PrivateKeyBlock.Bytes
	_, err := x509.ParsePKCS8PrivateKey(PrivateKeyBytes)
	if err != nil {
		log.Fatalf("Unable to parse private RSA key")
	}
}

func readCurrentKeyBlock(options cli.Options) *pem.Block {
	if _, err := os.Stat(options.PKI.PrivKeyPath); err == nil {
		PrivateKey, fileErr := ioutil.ReadFile(options.PKI.PrivKeyPath)
		if fileErr != nil {
			log.Fatalf("Unable to read Private Key at %s: %s", options.PKI.PrivKeyPath, fileErr)
		}

		pemBlock, _ := pem.Decode([]byte(PrivateKey))
		if pemBlock == nil {
			log.Fatalf("Failed to decode Private Key")
		}

		return pemBlock
	}
	return nil
}

func isPKCS1Block(blockType string) bool {
	return blockType == string(certutil.PKCS1Block)
}

func isPKCS8Block(blockType string) bool {
	return blockType == string(certutil.PKCS8Block)
}

func getFileHash(file string) string {
	f, err := os.Open(file)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	h := md5.New()
	if _, err := io.Copy(h, f); err != nil {
		log.Fatal(err)
	}
	return fmt.Sprintf("%x", h.Sum(nil))
}

func ExtendIPSAN(slice []net.IP, element net.IP) []net.IP {
	n := len(slice)
	if n == cap(slice) {
		// Slice is full; must grow.
		// We double its size and add 1, so if the size is zero we still grow.
		newSlice := make([]net.IP, len(slice), 2*len(slice)+1)
		copy(newSlice, slice)
		slice = newSlice
	}
	slice = slice[0 : n+1]
	slice[n] = element
	return slice
}
