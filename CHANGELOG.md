# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.8.0] - 2019-04-21

### Fixed

- build vaultbot based on go modules (see [#16](https://gitlab.com/msvechla/vaultbot/issues/16))

### Added

- allow specification of `pki_private_key_format` (PR by @zer0beat 👏)

## [1.7.0] - 2019-04-10

### Fixed

- PEM bundle file permissions are more strict now: `0600` (see [#12](https://gitlab.com/msvechla/vaultbot/issues/12))
- automated tests now work on branches of forks
- automated tests are now executed against `vault:1.1.0` and `go:1.12.1-stretch` (see [#11](https://gitlab.com/msvechla/vaultbot/issues/11))
- `CommonName` and `SAN` checks should not be case-sensitive (PR by @jflombardo 👏 & see [#13](https://gitlab.com/msvechla/vaultbot/issues/13))

### Added

- support for managing certificates in JAVA Keystores (PR by @jflombardo 👏)

## [1.6.0] - 2019-01-25

### Fixed

- correctly use supplied `ClientKey` option (PR by @fuerob 👏)

### Added

- `cert` authentication method (thanks @fuerob 👏)  

## [1.5.0] - 2018-12-23

### Added

- `approle` authentication method (see [#9](https://gitlab.com/msvechla/vaultbot/issues/9))  

## [1.4.0] - 2018-12-11

### Added

- `--version` flag to print current vaultbot version (see [#8](https://gitlab.com/msvechla/vaultbot/issues/8))

## [1.3.2] - 2018-12-10

### Fixed

- vaultbot falsely claimed that certificate data changed on certificates with empty dns- and ip sans (see [#7](https://gitlab.com/msvechla/vaultbot/issues/7))

### Added

- test with empty dns- and up sans on the to-be-updated certificate

## [1.3.1] - 2018-11-11

### Fixed

- write AWS EC2 Auth nonce to file, if it has not been set via CLI flag. Based on PR by @eljas 👏
- correct vault version output on startup

## [1.3.0] - 2018-11-04

### Added

- support for vault `AWS EC2` and `AWS IAM` authentication (see [#4](https://gitlab.com/msvechla/vaultbot/issues/4))
- introduced `go mod`
- first refactoring into packages to improve maintainability

## [1.2.2] - 2018-09-18

### Fixed

- renamed incorrect environment variable names for `PKI_COMMON_NAME` and `PKI_RENEW_PERCENT`. PR by @fuerob 👏
- markdown lint errors in CHANGELOG.md

## [1.2.1] - 2018-04-11

### Fixed

- error handling for parsing certificates
- bumped to latest go version

## [1.2.0] - 2018-03-12

### Added

- `pem_bundle` option, to specify an output location of the certificate chain and private key as PEM bundle

## [1.1.0] - 2018-01-24

### Added

- Use `renew_hook` to specify a command which will be executed after a successful certificate renewal
- JSON logging using [logrus](https://github.com/sirupsen/logrus)
- Logging to file by specifying the `logfile` option

## [1.0.2] - 2018-01-23

### Fixed

- Fixed initial cert request not working as expected

### Added

- End to end tests to catch obvious errors
- CA-Certificates to docker image
- Push all docker tags according to SemVer

## [1.0.1] - 2018-01-22

### Fixed

- Fixed build pipeline for all major platforms

## [1.0.0] - 2018-01-21

### Added

- Initial version release
- Request new certificates
- Expiry checks by percent and fixed time
- Sanity checks before overwriting files
- Complete tests with vault in Docker
- CI Pipeline
- README
- CHANGELOG
- ...
